from collections import defaultdict

elves = {}
with open('d23.txt') as f:
    for j, line in enumerate(f):
        for i, char in enumerate(line):
            if char == "#":
                elves[(i, j)] = (i, j)

def add_coord(*args):
    return tuple(map(sum, zip(*args)))

dir = {
    ( 0, -1): [(-1, -1), ( 0, -1), ( 1, -1)],
    ( 0,  1): [(-1,  1), ( 0,  1), ( 1,  1)],
    (-1,  0): [(-1, -1), (-1,  0), (-1,  1)],
    ( 1,  0): [( 1,  1), ( 1,  0), ( 1, -1)],
}
dir2 = [( 0, -1), ( 0,  1), (-1,  0), ( 1,  0)]

neis = [(i, j) for i in [-1, 0, 1] for j in [-1, 0, 1] if (i, j) != (0, 0)]

# for round in range(10):
round = 0
while True:
    round += 1
    old_positions_set = set(elves.keys())
    new_positions = defaultdict(list)
    for elf in elves:
        if all(add_coord(elf, nei) not in elves for nei in neis):
            continue
        for n in range(4):
            d = dir2[n]
            if any(add_coord(elf, n) in elves for n in dir[d]):
                continue
            new_pos = add_coord(elf, d)
            elves[elf] = new_pos
            new_positions[new_pos].append(elf)
            break
    for new_pos in new_positions:
        nps = new_positions[new_pos]
        if len(nps) > 1:
            for elf in nps:
                elves[elf] = elf
    elves = {v: v for k, v in elves.items()}
    new_positions_set = set(elves.keys())
    # print(elves.keys())
    dir2 = dir2[1:] + [dir2[0]]
    if round == 10:
        maxes = list(map(max, zip(*elves.keys())))
        mins  = list(map(min, zip(*elves.keys())))
        diffs = maxes[0] - mins[0], maxes[1] - mins[1]
        print((diffs[0]+1) * (diffs[1]+1) - len(elves))
        
        # break
    if old_positions_set - new_positions_set == set():
        print(round)
        break
