from dataclasses import dataclass

with open('d11.txt') as f:
    monkeys = "".join([line for line in f.readlines()]).split("\n\n")

divisible = 1

@dataclass
class Monkey():
    n: int
    items: list[int]
    rule: str
    test: int
    tr: int
    fl: int
    inspected: int = 0

for i, monkey in enumerate(monkeys):
    monkey = monkey.split('\n')
    n = int(monkey[0][6:-1])
    items = [int(x) for x in monkey[1].split(':')[1].split(',')]
    rule = monkey[2].split(':')[1].strip()
    test = int(monkey[3].split(' ')[5])
    tr = int(monkey[4].split(' ')[9])
    fl = int(monkey[5].split(' ')[9])
    monkeys[i] = Monkey(n, items, rule, test, tr, fl)
    divisible *= test

# print(monkeys)
for k in range(10000):
    for monk in monkeys:
        for _ in range(len(monk.items)):
            old = monk.items.pop()
            monk.inspected += 1
            exec(monk.rule)
            # new = new // 3 
            if new % monk.test:
                monkeys[monk.fl].items.append(new % divisible) 
            else:
                monkeys[monk.tr].items.append(new % divisible)

x = sorted([m.inspected for m in monkeys])[::-1]
print(x, x[0]*x[1])