from queue import PriorityQueue
import networkx as nx

with open('d12.txt') as f:
    lines = [line.strip() for line in f.readlines()]


D = dict()
maxX = len(lines[0])
maxY = len(lines)
def neighbours(tile):
    x, y = tile
    nei = []
    for x1, y1 in [(x + 1, y), (x - 1, y), (x, y + 1), (x, y - 1)]:
        if 0 <= x1 < maxX and 0 <= y1 < maxY:
            nei.append((x1, y1))
    return nei

def findPath(source:tuple, destination:tuple):
    global D
    """A* path from source to destination"""
    cost = {source: 0}
    frontier = PriorityQueue()
    for tile in D[source]['neighbours']:
        frontier.put((1, tile))
        cost[tile] = 1
    
    while not frontier.empty():
        _, current = frontier.get()
        
        if current == destination:
            return cost[current]
        
        for next in D[current]['neighbours']:
            next_cost = cost[current] + 1
            if next not in cost or next_cost < cost[next]:
                cost[next] = next_cost
                priority = next_cost
                frontier.put((priority, next))
    else:
        # print('No path??')
        pass

for j, line in enumerate(lines):
    for i, chr in enumerate(line):
        coord = (i, j)
        if chr == 'S':
            start = coord
            D[coord] = {'chr': ord('a'), 'neighbours': []}
        elif chr == 'E':
            end = coord
            D[coord] = {'chr': ord('z'), 'neighbours': []}
        else:
            D[coord] = {'chr': ord(chr), 'neighbours': []}

for tile in D:
    for nei in neighbours(tile):
        if D[nei]['chr'] <= D[tile]['chr'] or D[nei]['chr'] == D[tile]['chr'] + 1:
            D[tile]['neighbours'].append(nei)
    # print(D[tile])
"""part 1"""
p, P = findPath(start, end)
print(p)
print(len(P))

# broteforce
# s = maxX * maxY
# for tile in D:
#     if D[tile]['chr'] == ord('a'):
#         p = findPath(tile, end)
#         if p is not None:
#             s = min(s, p)
        
# print(s)

# import numpy as np
# import matplotlib.pyplot as plt

# data = [[D[(x, y)]['chr'] for x in range(maxX)] for y in range(maxY)]

# plt.imshow(data, interpolation="none")
# plt.show()