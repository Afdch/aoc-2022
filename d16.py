import re
import networkx as nx

pr = r'Valve ([A-Z]{2}) has flow rate=(\d*); tunnels? leads? to valves? (.+)'
valves = {}
G = nx.Graph()
for line in open("d16.txt").readlines():
    valve, volume, tunnels = re.findall(pr, line.strip())[0]
    tunnels = tunnels.split(', ')
    G.add_node(valve, value=int(volume), on=False)
    for tunnel in tunnels:
        G.add_edge(valve, tunnel)

global_max = 0
def random_switch(current, switched, time, value):
    global global_max
    current = sorted(current, key=lambda x: x[1], reverse=True)
    actor_pos, actor_time = current[0]
    if actor_time < time:
        time = actor_time
    for node in G.nodes:
        if node in switched:
            continue
        if G.nodes[node]['value'] == 0:
            continue
        path = nx.shortest_path(G, actor_pos, node)
        actor_time = time - len(path)
        if actor_time <= 0:
            continue
        value1 = value + actor_time * G.nodes[node]['value']
        random_switch(current[1:] + [(node, actor_time)], switched + [node], time, value1)
    global_max = max(global_max, value)
        
    
    
random_switch([("AA", 30)], [], 30, 0)
print(global_max)
global_max = 0
random_switch([("AA", 26),("AA", 26)], [], 26, 0)
print(global_max)