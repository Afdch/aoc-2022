conv = {"=": -2,
     "-": -1,
     "0": 0,
     "1": 1,
     "2": 2,
     0: "0",
     1: "1",
     2: "2",
     -1: "-",
     -2: "="
}

def base5_10(s: str):
    len_s = len(s)
    return sum(conv[x]*(5**(len_s - i - 1)) for i, x in enumerate(s))

def base10_5(n: int):
    d, m = divmod(n, 5)
    if m > 2:
        m -=5
        d += 1
    if d == 0:
        return conv[m]
    else:
        return  base10_5(d) + conv[m]
    
summ = 0

with open('d25.txt') as f:
    for line in f:
        n5 = base5_10(line.strip())
        # print(line.strip(), n5, base10_5(n5))
        summ += n5
        
print(base10_5(summ))