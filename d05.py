with open('d05.txt') as f:
    l = "".join(line for line in f)

s = []
setup, tasks = l.split('\n\n')
for stp in setup.split('\n'):
    stp = stp[1::4]
    # print(stp)
    s.append(stp)
# print(s)
z = []
for col in range(len(s[-1])):
    c = []
    for d in range(len(s) - 1, 0, -1):
        x = s[d-1][col]
        if x != ' ':
            c.append(x)
    # print(c)
    z.append(c)

#  print(z)
tasks = tasks.split('\n')
for task in tasks:
    task = task.split()
    
    amount = int(task[1])
    frm = int(task[3]) - 1
    to = int(task[5]) - 1
    # print(amount, frm, to)
    
    """part 1"""
    # for _ in range(amount):
    #     z[to].append(z[frm].pop())
    #     # print(z)
    
    """part 2"""
    z[frm], take = z[frm][:-amount], z[frm][-amount:]
    z[to] = z[to] + take


print("".join(x[-1] for x in z))