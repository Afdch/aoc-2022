ops = {}
with open('d21.txt') as f:
    for line in f:
        key, op = line.strip().split(': ')
        ops[key] = op

"""part 1"""
def opa(op):
    operator = ops[op]
    if operator.isdigit():
        return int(operator)
    op1, operation, op2 = operator.split(' ')
    if operation == "/":
        return opa(op1) // opa(op2)
    return eval(f"{opa(op1)}{operation}{opa(op2)}")        
print(opa("root"))

"""part 2"""
def unwrap(op) -> str:
    operator: str = ops[op]
    if op == "humn":
        return op
    if operator.isdigit():
        return operator
    op1, oper, op2 = operator.split(' ')
    if op == "root":
        return f"{unwrap(op1)} - {unwrap(op2)}"
    return f"({unwrap(op1)} {oper} {unwrap(op2)})"

f = unwrap("root")
    
    
"""assume a positive answer"""
def iterate(start, stop, step):
    for humn in range(start, stop, step):
        f_h = eval(f)
        # print(humn, f_h)
        if f_h < 0:
            f_h = iterate(humn - step, humn, step // 100)
        if f_h == 0:
            print(humn)
            return 0

iterate(0, 100**10, 100**5 )