lines = []

def get_range_set(r: str):
    lst = [int(x) for x in r.split('-')]    
    return set(range(lst[0], lst[1] + 1))

with open('d04.txt') as f:
    for line in f:
        a, b = line.strip().split(',')
        a1, b1 = get_range_set(a), get_range_set(b)
        
        lines.append((a1, b1))
   
i = 0
j = 0

# wtf I'm doing it wasn't the task :(
# for line1 in lines:
#     for line2 in lines:
#         if line1 is not line2:
#             if (line2[0] >= line1[0]) and (line2[1] >= line1[0]):
#                 print (line1, line2)
#                 i += 1
# print(i)

for a, b in lines:
    if a <= b or b <= a:
        i += 1
    if len(a&b) > 0:
        j += 1
    
print(i)
print(j)