cubes = {tuple(map(int, l.split(','))): 6 for l in open('d18.txt')}

def sum3d(a, b): return a[0] + b[0], a[1] + b[1], a[2] + b[2]

def get_neighbours(cube):
    return [sum3d(cube,nei) for nei in [(1, 0, 0), (-1, 0, 0),
                                        (0, 1, 0), (0, -1, 0),
                                        (0, 0, 1), (0, 0, -1)]]

def count_neighbours(cube):
    return sum([1 for nei in get_neighbours(cube) if nei in cubes])
x, y, z = zip(*cubes)
maxX, maxY, maxZ = max(x), max(y), max(z)

"""part 2"""
for x1 in range(maxX):
    for y1 in range(maxY):
        for z1 in range(maxZ):
            if (c:=(x1, y1, z1)) not in cubes:
                oob = False
                neis = [n for n in get_neighbours(c) if n not in cubes]
                e = []
                if len(neis) == 6:
                    continue
                if len(neis) == 0:
                    cubes[c] = 0
                while len(neis) > 0 and not oob:
                    t = neis.pop()
                    e.append(t)
                    for nei in get_neighbours(t):
                        if nei in cubes:
                            continue
                        x, y, z = nei
                        if x < 0 or x > maxX or y < 0 or y > maxY or z < 0 or z > maxZ:
                            oob = True
                            break
                        if nei not in neis and nei not in e:
                            neis.append(nei)
                    if (0, 0, 0) in neis:
                        break
                if not oob:
                    for t in e:
                        cubes[t] = 0
for cube in cubes:
    cubes[cube] = 6 - count_neighbours(cube)

print(sum(cubes.values()))