def compare(p1, p2):
    if type(p1) is int:
        if type(p2) is int:
            if p1 == p2:
                return None
            return p1 < p2
        else:
            return compare([p1], p2)
    else:
        if type(p2) is int:
            return compare(p1, [p2])
        else:
            # both are lists
            for i in range(max(len(p1), len(p2))):
                if i >= len(p1) and i < len(p2):
                    return True
                if i >= len(p2) and i < len(p1):
                    return False

                c = compare(p1[i], p2[i])
                if c is not None:
                    return c
"""p1"""
with open('d13.txt') as f:
    pairs = [[eval(x) for x in pair.split()] for pair in "".join([x for x in f]).split('\n\n')]

# print([compare(p1, p2) for p1, p2 in pairs])

print(sum([i+1 for i, x in enumerate([compare(p1, p2) for p1, p2 in pairs]) if x]))

"""p2"""
divid = [[[2]],[[6]]]
with open('d13.txt') as f:
    lists = [eval(x.strip()) for x in f.readlines() if x.strip() != ""] + divid

from functools import cmp_to_key
r = sorted(lists, key=cmp_to_key(lambda x, y: 1 if compare(x, y) else -1), reverse=True)
ind = [i+1 for i, x in enumerate(r) if x in divid]
print(ind[0] * ind[1])
