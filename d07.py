with open('d07.txt') as f:
    lines = [line.strip() for line in f.readlines()]

fs = {'root': {}}
pointer = ""
for l in lines:
    match l.split():
        case "$", 'cd', other:
            match other:
                case "/":
                    pointer = f"fs['root']"
                case "..":
                    pointer = "[".join([x for x in pointer.split("[")][:-1])
                case letter:
                    pointer += f"['{letter}']"
                    # print(pointer)
        case "$", 'ls':
            pass
        case "dir", letter:
            exec(f"{pointer}.update({{'{letter}':{{}}}})")
        case size, filename:
            exec(f"{pointer}.update({{'{filename}': int(size)}})")

directories = []

def getsize(d: dict) -> int:
    s = 0
    for k in d.keys():
        if type(d[k]) == dict:
            s1 = getsize(d[k])
            directories.append(s1)
            s += s1
        else:
            s += d[k]
    return s

root_size = getsize(fs)
"""part 1"""
print(sum(x for x in directories if x < 100000))
"""part 2"""
need = root_size - 40000000
print(f'have {70000000 - root_size} free space, need 30000000, {need} to go')
for size in sorted(directories):
    if size > need:
        print(size)
        break