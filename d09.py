import numpy as np
links = 10 # 2 for part 1, 10 for part 2
head = [np.array([0,0]) for _ in range(links)]

dir = {
    'R': np.array([ 1, 0]),
    'L': np.array([-1, 0]),
    'U': np.array([ 0, 1]),
    'D': np.array([ 0,-1]),
}

visited = set()

with open('d09.txt') as f:
    for line in f:
        direction, amnt = line.strip().split()
        for _ in range(int(amnt)):
            head[0] = head[0] +  dir[direction]
            for i in range(links-1):
                delta = head[i] - head[i+1]
                if max(abs(delta)) > 1:    
                    head[i+1] += np.sign(delta)
            visited.add(tuple(head[-1]))

print(len(visited))
