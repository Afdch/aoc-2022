import aoc

D = aoc.Maze(open('d12.txt'))

"""get starting end ending points"""
S, E = D.features['S'][0], D.features['E'][0]
D.tiles[S] = 'a'
D.tiles[E] = 'z'

"""overwrite default rules for neighbours"""
for tile in D.tiles:
    D.neighbours[tile] = [x for x in D.neighbours[tile] if 
                            ord(D.tiles[tile]) + 1 >= ord(D.tiles[x])] 

"""A*: part 1 answer"""
P1 = D.findPath(S, E, 'path')
print(len(P1) - 1)

"""Draw a plot"""
import matplotlib.pyplot as plt
P = list(zip(*P1))
data = [[ord(D.tiles[(x, y)]) for x in range(D.maxX + 1)] for y in range(D.maxY + 1)]
plt.imshow(data, interpolation="none")
plt.plot(*P, color='red')
plt.show()