p2 = {
    'X': 1,
    'Y': 2,
    'Z': 3,
}

win = {
    'X': {'A': 3, 'B': 0, 'C': 6},
    'Y': {'A': 6, 'B': 3, 'C': 0},
    'Z': {'A': 0, 'B': 6, 'C': 3},
}


win2 = {
    'X': {'A': 3, 'B': 1, 'C': 2},
    'Y': {'A': 4, 'B': 5, 'C': 6},
    'Z': {'A': 8, 'B': 9, 'C': 7},
}


score = 0
score2 = 0
with open('d02.txt') as f:
    for line in f:
        P1, P2 = line.strip().split()
        s1 = p2[P2]
        s2 = win[P2][P1]
        score += p2[P2] + win[P2][P1]

        score2 += win2[P2][P1]
        # print(s1, s2, P1, P2)

print(score)
print(score2)

