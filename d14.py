tiles = {}

with open('d14.txt') as f:
    lines = f.read().split('\n')

for line in lines:
    line = [[int(x) for x in l.split(',')] for l in line.split(' -> ')]
    # print (line)
    pointer = line[0]
    for i in range(len(line) - 1):
        x1, y1 = line[i]
        x2, y2 = line[i+1]
        if x1 - x2 != 0:
            a, b = sorted([x1, x2])
            # print(a, b)
            for x in range(a, b+1, 1):
                tiles[(x, y1)] = "#"
        else:
            a, b = sorted([y1, y2])
            # print(a, b)
            for y in range(a, b+1, 1):
                tiles[(x1, y)] = "#"
# print(tiles)

def add_tuple(a, b):
    return (a[0] + b[0], a[1] + b[1])

def check_bottom(tile):
    if tile is None:
        return True
    x1, y1 = tile
    Z = any([x1 == x2 and y2 > y1 for x2, y2 in tiles]) 
    return Z

def check_next(next):
    global make_sand
    if next[1] == maxY + 2:
        tiles[next] = '#'
    else:
        make_sand = next

has_bottom = True
make_sand = (500, 0)
new_sand = 0
maxY = max([y for _, y in tiles])

while has_bottom:
    """part 1 condition"""
    # has_bottom = check_bottom(make_sand)
    
    if make_sand is None:
        make_sand = (500, 0)
        new_sand += 1
        if make_sand in tiles:
            break
    next = add_tuple(make_sand, (0, 1))
    if next not in tiles:
        check_next(next)
        continue
    next = add_tuple(make_sand, (-1, 1))
    if next not in tiles:
        check_next(next)
        continue
    next = add_tuple(make_sand, (1, 1))
    if next not in tiles:
        check_next(next)
        continue
    tiles[make_sand] = 'o'
    make_sand = None

print(new_sand)