import numpy as np
with open('d08.txt') as f:
    trees = np.array([[int(c) for c in line.strip()] for line in f.readlines()])
print(trees)

high = 0
for i, line in enumerate(trees):
    for j, c in enumerate(line):
        if i == 0 or j == 0 or i == len(trees)-1 or j == len(line)-1:
            high += 1
            continue
        if any([
            trees[(j, i)] > max(trees[:j,i]),
            trees[(j, i)] > max(trees[j,:i]),
            trees[(j, i)] > max(trees[j+1:,i]),
            trees[(j, i)] > max(trees[j,i+1:])
        ]):
            high += 1        
print(high)


scenic = 0
for i, line in enumerate(trees):
    for j, c in enumerate(line):
        sc = [0,0,0,0]
        for k, los in enumerate([trees[:j,i][::-1], trees[j,:i][::-1],trees[j+1:,i],trees[j,i+1:]]):
            for tree in los:
                sc[k] += 1
                if trees[(j, i)] <= tree:
                    break
        sc = sc[0] * sc[1] * sc[2] * sc[3]
        if sc > scenic:
            scenic = sc
print(scenic)

