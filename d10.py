with open('d10.txt') as f:
    lines = [line.strip() for line in f.readlines()]

display = [[" " for x in range(40)] for y in range(6)]

X = 1
addx_hanging = False
X1 = 0
v = []
    
for cycle in range(1, 241):
    if cycle in [20, 60, 100, 140, 180, 220]:
        v.append(X*cycle)
    
    cy, cx = divmod(cycle-1, 40)
    if cx in [X-1,X,X+1]:
        display[cy][cx] = "#"
    
    if not addx_hanging:
        line = lines.pop(0)
        match line.split():
            case "noop":
                pass
            case "addx", val:
                X1 = int(val)
                addx_hanging = True
    else: 
        addx_hanging = False
        X += X1
        X1 = 0

print(sum(v))

for i in display:
    print("".join(i))