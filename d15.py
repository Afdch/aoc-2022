sensors = []
beacons = {}
dist = {}

sensed = set()

def manh(a, b):
    return abs(a[0]-b[0])+abs(a[1]-b[1])

def section(origin, radius, section):
    # abs(X-X0) + abs(O[1]-section) = radius
    if abs(origin[1] - section) > radius:
        # print(origin[1], abs(origin[1] - section), radius)
        return None
    X1 = radius - abs(origin[1] - section)
    X = sorted([origin[0] - X1, origin[0] + X1])
    # r = set(range(X[0], X[1]+1))
    return X

with open('d15.txt') as f:
    for line in f:
        s_x, s_y, b_x, b_y = [int(x[2:]) for x in line.strip().replace(':',' ').replace(',', ' ').split(' ') if "=" in x]
        beacons[(s_x, s_y)] = (b_x, b_y)
        dist[(s_x, s_y)] = manh((s_x, s_y), beacons[(s_x, s_y)])
        # Z = section((s_x, s_y), dist[(s_x, s_y)], 2000000)
        # if Z is not None:
        #     sensed |= set(range(Z[0], Z[1]+1))

# lol why whould I do that
# for sensor in beacons:
#     # s_b, dist = sorted([(b, manh(sensor, b)) for b in beacons], key=lambda item: item[1])[0]
#     # print(sensor, s_b, dist)
#     Z = section(sensor, manh(sensor, beacons[sensor]), 2000000)
#     if Z is not None:
#         sensed |= set(range(Z[0], Z[1]+1))

# for x, y in beacons.values():
#     if y == 2000000:
#         sensed -= {x}

# print(len(sensed))

def join(S):
    while len(S) > 1:
        if S[1][0] <= S[0][1] <= S[1][1]:
            S[1][0] = S[0][0]
            S.pop(0)
        elif S[0][1] > S[1][1]:
            S.pop(1)
        elif S[0][1] < S[1][0]:
            Z = join(S[1:])
            if Z[0][0] - S[0][1] == 1:
                return [[S[0][0], Z[0][1]]]
            else:
                return [S[0]] + Z
    return S

for y in range(4000000):
    if y % 100000 == 0:
        print(y)
    c = sorted([x for x in [section(s, manh(s, beacons[s]), y) for s in beacons] 
        if x is not None])
    # print(c)
    # print(x, join(c))
    V = join(c)
    if len(V) > 1:
        print(V, y)
        print((V[0][1]+1) * 4000000 + y)
        break