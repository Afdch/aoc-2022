with open('d03.txt') as f:
    lines = [line.strip() for line in f.readlines()]

def score(char: str) -> int:
    scr = ord(char) - 96
    if scr <= 0:
        scr += 58
    return scr

def chunks(lst: list, n: int) -> list:
    """Yield successive n-sized chunks from lst."""
    for i in range(0, len(lst), n):
        yield lst[i:i + n]

"""part 1"""
sum = 0        
for line in lines:
    l = len(line) // 2
    a, b = set(line[0:l]), set(line[l:])
    sum += score(*a&b)
print(sum)

"""part 2"""
sum = 0        
for chunk in chunks(lines, 3):
    sum += score(*set.intersection(*(set(x) for x in chunk)))
print(sum)